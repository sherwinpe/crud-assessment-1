<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CRUD</title>

        <!-- Laravel Style -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

        <!-- Laravel Javascript -->
        <script src="{{ asset('js.app.js') }}"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Laravel CRUD Exam</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                    
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/">Guide Lines</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/posts">Posts List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/posts/create">New Posts</a>
                    </li>
                </ul>
            </div>
        </nav>

        <br>

        <div class="container">
            @yield("content")
        </div>
    </body>
</html>
