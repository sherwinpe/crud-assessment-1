<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Set column name for record creation timestamp
     * 
     * @var string
     */
    const CREATED_AT = "AddedOn";

    /**
     * Set column name for record creation timestamp
     * 
     * @var string
     */
    const UPDATED_AT = "UpdatedOn";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine      = 'InnoDB';
            $table->charset     = 'utf8';
            $table->collation   = 'utf8_unicode_ci';
            $table->increments('ID');
            $table->string('PostNo', 13);
            $table->string('Writer', 150);
            $table->string('Subject', 150);
            $table->text('Body');
            $table->string("Attachment", 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
